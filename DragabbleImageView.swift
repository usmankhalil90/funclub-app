//
//  DragabbleImageView.swift
//  FunClub
//
//  Created by NISUM on 7/13/17.
//  Copyright © 2017 Technology-minds. All rights reserved.
//

import UIKit

class DragabbleImageView: UIImageView, UIGestureRecognizerDelegate {
    var longPressGesture: UILongPressGestureRecognizer?
    var pinchGesture: UIPinchGestureRecognizer?
    var rotationGesture: UIRotationGestureRecognizer?
    
    var currentPoint = CGPoint.zero
    
    override init(image: UIImage?) {
        super.init(image: image)
        
        isUserInteractionEnabled = true
        longPressGesture = UILongPressGestureRecognizer()
        pinchGesture = UIPinchGestureRecognizer()
        rotationGesture = UIRotationGestureRecognizer()
        addGestureRecognizer(longPressGesture!)
        addGestureRecognizer(pinchGesture!)
        addGestureRecognizer(rotationGesture!)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        currentPoint = (touches.first?.location(in: self))!
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let activePoint: CGPoint? = touches.first?.location(in: self)
        // new point based on where the touch is now located
        var newPoint = CGPoint(x: center.x + ((activePoint?.x)! - currentPoint.x), y: center.y + ((activePoint?.y)! - currentPoint.y))
        let midPointX: Float = Float(bounds.midX)
        let pointX = Float((superview?.bounds.size.width)!) - midPointX
        if newPoint.x > CGFloat(pointX) {//superview?.bounds.size.width - midPointX {
            newPoint.x = CGFloat(pointX)//superview?.bounds.size.width - midPointX
        }
        else if newPoint.x < CGFloat(midPointX) {
            newPoint.x = CGFloat(midPointX)
        }
        
        let midPointY: Float = Float(bounds.midY)
        let pointY = Float((superview?.bounds.size.height)!) - midPointY

        if newPoint.y > CGFloat(pointY) {//superview?.bounds.size.height - midPointY {
            newPoint.y = CGFloat(pointY)//superview?.bounds.size.height - midPointY
        }
        else if newPoint.y < CGFloat(midPointY) {
            newPoint.y = CGFloat(midPointY)
        }
        
        //new center location
        center = newPoint
    }
    
    // MARK: Gesture Reconizer Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
